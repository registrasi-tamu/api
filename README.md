# API REGISTRASI TAMU

This project was generated with nodeJs version 12.18.4

# Getting Started

Before you setup the application, make sure you have the prerequisites:
- [NodeJS](https://nodejs.org/en/) 12.18.4
- [Mysql](https://www.mysql.com/)
- [NPM](https://www.npmjs.com/) 6.14.6


### Install Dependencies

```bash
$ npm install
```

### Running the application
```bash
nodemon start 
```

### run pm2
```bash
pm2 start index.js
```

### it is running with port 3000

