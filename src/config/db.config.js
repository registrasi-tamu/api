/**
 * created at :22/08/2022
 * created by :muhammad agus suryadi
 * desc       :configuration connection database
 */

 const Sequelize = require("sequelize");
 require('dotenv').config()
 const sequelize = new Sequelize(process.env.DB_NAME,process.env.DB_USER,process.env.DB_PASS,{
   host    : process.env.DB_HOST,
   dialect : "mysql",
   pool    :{max : 5,min : 0,idle: 10000}
 });
 module.exports = sequelize;