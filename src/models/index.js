/**
 * created at :22/08/2022
 * created by :muhammad agus suryadi
 * desc       :routes models database
 */

 const models = {}
 models.user              = require("./user");
 models.guestBook         = require("./guestBook");
 module.exports = models;
 