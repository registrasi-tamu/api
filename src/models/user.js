/**
 * created at :22/08/2022
 * created by :mmuhammad agus suryadi
 * desc       :model user
 */

 const 
 Sequelize = require("sequelize"),
 user      = require("../config/db.config").define('user',{ //define table from database
     'id'              :{type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
     'username'        : Sequelize.STRING(45),
     'password'        : Sequelize.STRING(300),
     'name'            : Sequelize.STRING(45),
     'createdAt'       : {type: Sequelize.DATE, defaultValue: Sequelize.NOW,  field: 'created_at'},    
     'updatedAt'       : {type: Sequelize.DATE, defaultValue: Sequelize.NOW,  field: 'updated_at'}
   },{
     //prevent sequelize transform table name into plural
     freezeTableName: true,
   }
 ); 
 module.exports = user;
 
 