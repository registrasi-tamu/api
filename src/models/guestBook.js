/**
 * created at :22/08/2022
 * created by :mmuhammad agus suryadi
 * desc       :model user
 */

 const 
 Sequelize = require("sequelize"),
 guest      = require("../config/db.config").define('guest',{ //define table from database
     'id'              :{type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
     'id_card_number'  : Sequelize.STRING(45),
     'card_type'       : Sequelize.STRING(20),
     'name'            : Sequelize.STRING(45),
     'address'         : Sequelize.STRING,
     'phone_number'    : Sequelize.STRING(15),
     'necessary'       : Sequelize.STRING,
     'card_number'     : Sequelize.STRING(50),
     'status'          : Sequelize.STRING(10),
     'createdAt'       : {type: Sequelize.DATE, defaultValue: Sequelize.NOW,  field: 'created_at'},    
     'updatedAt'       : {type: Sequelize.DATE, defaultValue: Sequelize.NOW,  field: 'updated_at'}
   },{
     //prevent sequelize transform table name into plural
     freezeTableName: true,
   }
 ); 
 module.exports = guest;
 
 