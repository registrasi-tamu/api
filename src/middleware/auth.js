/**
 * created at :22/08/2022
 * created by :muhammad agus suryadi
 * desc       :function verify token
 */
//token verification
const response  = require("./response");
require('dotenv').config();
app.set("ss", process.env.APP_KEY);
let authToken =(req,res,next)=>{
  try{
    let tokenCode = req.headers['authorization'];
    if(tokenCode){
      tokenCode = tokenCode.split(" ");
      if(tokenCode[0] == "Bearer" && tokenCode[1]){
          require("jsonwebtoken").verify(tokenCode[1], app.get("ss"), (err,decode)=>{    
          if(err){
            return response.failed(res, "Invalid token");
          }
          next();
        });
      }else{
        return response.failed(res, "Invalid token");
      }
    }else{ 
      return response.failed(res, "Invalid token");
    }
  }catch(err){
    return response.failed(res, err.message || "Gagal memuat data.");
  }
}
module.exports = authToken;
//end verify token