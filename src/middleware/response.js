/**
 * created at :22/08/2022
 * created by :muhammad agus suryadi
 * desc       :middleware response api
 */

 const response = function (){};
 response.success = async(res, message, data, total) => {
   return res.status(200).json({
     status: 200,
     directoryFile: global.directoryFile,
     message,
     data: data,
     total: total,
   });
 }
 response.failed = async(res, message) => {
   return res.status(200).json({
     status: 500,
     message,
   });
 }
 response.notfound = async(res, message = "url tidak dikenali sistem") => {
   return res.status(200).json({
     status: 404,
     message,
   });
 }
 module.exports = response;