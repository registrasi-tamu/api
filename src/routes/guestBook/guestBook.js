/**
 * created at :22/08/2022
 * created by : muhammad agus suryadi
 * desc : routes function guest
 */

 const 
 express   = require("express"),
 router    = express.Router(),
 models    = require("../../models/index"),
 response  = require("../../middleware/response"),
 { Op } = require("sequelize"),
 {check, validationResult} = require('express-validator/check'); //form validation
 
 router.post("/", async (req, res, next)=>{ // request post 
   try{
     let errors = validationResult(req);
     if(!errors.isEmpty()) {
       return response.failed(res, errors.mapped());
     }
     await models.guestBook.create({
        id_card_number : req.body.id_card_number,
        card_type : req.body.card_type,
        name : req.body.name,
        address : req.body.address,
        phone_number : req.body.phone_number,
        necessary : req.body.necessary,
        card_number : req.body.card_number,
        status : req.body.status
     }).then(data => {
       return response.success(res, 'Success', data.dataValues);
     });
   }catch(err){
     return response.failed(res, err.message || "Gagal memuat data.");
   }
 });
 
 router.put("/:id",[
   check("id")
   .custom(value => {
     return models.guestBook.findOne({where: {id: value}}).then(b => {
       if(!b){
         throw new Error('id tidak ditemukan');
       }            
     })
   })
 ], async (req, res, next)=>{ // request update
   try{
     let errors = validationResult(req);
     if (!errors.isEmpty()) {
       return response.failed(res, errors.mapped());
     }
     if(req.params.id){
       let data_update = {
        id_card_number : req.body.id_card_number,
        card_type : req.body.card_type,
        name : req.body.name,
        address : req.body.address,
        phone_number : req.body.phone_number,
        necessary : req.body.necessary,
        card_number : req.body.card_number,
        status : req.body.status
       }
       await models.guestBook.update(data_update,{where: {id: req.params.id}})
       .then(affectedRow =>{
         return models.guestBook.findOne({where: {id: req.params.id}})      
       })
       .then(data =>{
         return response.success(res, "Success", data);
       });
     }else{
       return response.failed(res, "id tidak ditemukan");
     }
   }catch(err){
     return response.failed(res, err.message || "Gagal memuat data.");
   }
 });
 
 router.get("/:offset", (req, res, next)=>{ //request get all
  try{
     let src = (req.query? req.query : req.query);
     limit = (req.params.limit? req.params.limit: global.limit);
     offset = (req.params.offset? req.params.offset: global.offset);
     let whereData = {id:{[Op.gt]:0}};
     if (src.keyword || src.status) {
      whereData = {
        [Op.or]: [
          {name :{[Op.like]:`%${src.keyword}%`}},
          {id_card_number :{[Op.like]:`%${src.keyword}%`}},
          {card_type :{[Op.like]:`%${src.keyword}%`}},
          {address :{[Op.like]:`%${src.keyword}%`}},
          {phone_number :{[Op.like]:`%${src.keyword}%`}},
          {necessary :{[Op.like]:`%${src.keyword}%`}},
          {card_number :{[Op.like]:`%${src.keyword}%`}},
        ], 
        status : (src.status ? `${src.status}` : 'IN')
      };
    }
     models.guestBook.findAndCountAll({
       limit:parseInt(limit),offset:parseInt(offset),
       order:[['updatedAt', 'DESC']],
       where:whereData,
       include:[]
     }).then(data =>{
       return response.success(res, "success", data.rows, data.count);
     });
   }catch(err){
     return response.failed(res, err.message || "Gagal memuat data.");
   }
 });
 
 router.delete("/:id", (req, res, next)=>{ // request deleted
   try{
     if(req.params.id){
       models.guestBook.destroy({where: {id: req.params.id}})
       .then(data =>{
         return response.success(res, "Success", data);
       });
     }else{
       return response.failed(res, "data tidak ditemukan");
     }
   }catch(err){
     return response.failed(res, err.message || "Gagal memuat data.");
   }
 });
 
 module.exports = router;