/**
 * created at :22/08/2022
 * created by :muhammad agus suryadi
 * desc       :routes function
 */

 module.exports = app =>{
  const authToken = require("../middleware/auth");
  const withAuth = async (req, res, next)=>{authToken(req, res, next);}
  app.use("/login", [require("./user/auth")]);
  app.use("/guest-book", [withAuth, require("./guestBook/guestBook")]);
  app.use("/", (req, res, next)=>{require("../middleware/response").notfound(res);})
}