const 
express   = require("express"),
router    = express.Router(),
jwt       = require("jsonwebtoken"),
app       = express(),
bcrypt    = require('bcrypt'),
response  = require("../../middleware/response"),
Models    = require("../../models/index");

require('dotenv').config();
app.set("ss", process.env.APP_KEY);

router.post("/", async(req, res, next)=>{ // request post data
  // console.log(bcrypt.hashSync(req.body.password,10)) 
  try{
    await Models.user.findOne({where:{"username":req.body.username}}).then(data=>{
      if(data){
        if(bcrypt.compareSync(req.body.password, data.password)){
          const 
            payload = {check:  true},
            tokens = jwt.sign(payload, app.get('ss'), {/** expiresIn: "365d" */  });
          data.dataValues['token'] = tokens;
          return response.success(res, "success", data);
        }else{
          return response.failed(res, "Username dan password tidak cocok");
        }
      }else{
        return response.failed(res, "Username dan password tidak ditemukan");
      }
    });
  }catch(err){
    return response.failed(res, err.message || "Gagal memuat data.");
  }
});

module.exports = router;
