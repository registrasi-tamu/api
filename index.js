/**
 * created at :22/08/2022
 * created by :muhammad agus suryadi
 * desc       :index app
 */

//require module
const 
express     = require("express"),
bodyParser  = require("body-parser"),
cors        = require('cors')
app         = express(),
path        = require('path');

require('dotenv').config();

//global var
global.limit = process.env.DATA_LIMIT;
global.offset= process.env.DATA_LIMIT; 
global.directoryFile= process.env.DIRECTORY_FILE;

//use body parser
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true }));

// seting cors access
app.use(cors()); 
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "*");
  next();  
}); 

//require routes
require("./src/routes/index")(app);

//listen or run server
app.listen(process.env.PORT,()=>{console.log(`server run with port ${process.env.PORT}`)})
